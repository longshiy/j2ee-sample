package org.light4j.j2ee.sample.dao;

import org.light4j.j2ee.sample.model.UserEntity;

public interface IUserDao {
	public boolean isExist(UserEntity user);
}
