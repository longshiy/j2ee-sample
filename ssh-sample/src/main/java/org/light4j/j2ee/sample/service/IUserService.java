package org.light4j.j2ee.sample.service;

import org.light4j.j2ee.sample.model.UserEntity;

public interface IUserService {
	public boolean isExist(UserEntity user);
}
