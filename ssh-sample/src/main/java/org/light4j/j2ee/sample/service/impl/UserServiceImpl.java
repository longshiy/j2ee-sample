package org.light4j.j2ee.sample.service.impl;

import javax.annotation.Resource;

import org.light4j.j2ee.sample.dao.IUserDao;
import org.light4j.j2ee.sample.model.UserEntity;
import org.light4j.j2ee.sample.service.IUserService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Service("userService")
public class UserServiceImpl implements IUserService {
	@Resource
	private IUserDao userDao;

	@Override
	public boolean isExist(UserEntity user) {
		return userDao.isExist(user);
	}

}
