package org.light4j.j2ee.sample.service;

import java.util.List;

import org.light4j.j2ee.sample.dao.po.UserEntity;

/**
 * User Service接口
 * 
 * @author longjiazuo
 */
public interface UserService {
	
	/**
	 * 1.根据用户id获取用户信息
	 * 
	 * @author longjiazuo
	 */
	UserEntity getUserEntityById(String userId);

	/**
	 * 获取所有的用户信息
	 * 
	 * @author longjiazuo
	 */
	List<UserEntity> getUserEntities();

	/**
	 * 插入用户信息
	 * 
	 * @author longjiazuo
	 */
	UserEntity insertUserEntity(UserEntity userEntity);
}
