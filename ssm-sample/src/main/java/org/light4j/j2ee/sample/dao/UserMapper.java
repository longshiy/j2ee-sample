package org.light4j.j2ee.sample.dao;

import java.util.List;

import org.light4j.j2ee.sample.dao.po.UserEntity;

/**
 * User Mapper
 * 
 * @author longjiazuo
 */
public interface UserMapper {
	UserEntity getUserEntityById(String userId);  
    
    List<UserEntity> getUserEntities();  
      
    int insertUser(UserEntity userEntity); 
}
