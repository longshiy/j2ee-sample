package org.light4j.j2ee.sample.service.impl;

import java.util.List;

import org.light4j.j2ee.sample.dao.UserMapper;
import org.light4j.j2ee.sample.dao.po.UserEntity;
import org.light4j.j2ee.sample.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

	@Autowired  
    private UserMapper userMapper;  
  
    public UserMapper getUserMapper() {  
        return userMapper;  
    }  
  
    public void setUserMapper(UserMapper userMapper) {  
        this.userMapper = userMapper;  
    }  
  
    @Override  
    public UserEntity getUserEntityById(String userId) {  
        return this.userMapper.getUserEntityById(userId);  
    }  
  
    @Override  
    public List<UserEntity> getUserEntities() {  
        return this.userMapper.getUserEntities();  
    }  
  
    @Override  
    public UserEntity insertUserEntity(UserEntity userEntity) {  
        this.userMapper.insertUser(userEntity);  
  
        return getUserEntityById(userEntity.getUserId());  
    }  
  
}
